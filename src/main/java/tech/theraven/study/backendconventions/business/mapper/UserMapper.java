package tech.theraven.study.backendconventions.business.mapper;

import org.mapstruct.Mapper;
import tech.theraven.study.backendconventions.api.dto.UserCreateDto;
import tech.theraven.study.backendconventions.api.dto.UserDto;
import tech.theraven.study.backendconventions.domain.User;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDto toDto(User user);

    User toEntity(UserCreateDto dto);

}
