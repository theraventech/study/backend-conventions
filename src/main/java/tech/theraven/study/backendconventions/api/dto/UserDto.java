package tech.theraven.study.backendconventions.api.dto;

public record UserDto(Long id, String name, String phone, String email) {
}
