package tech.theraven.study.backendconventions.api.dto;

public record UserCreateDto(String name, String phone, String email) {
}
