package tech.theraven.study.backendconventions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendConventionsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendConventionsApplication.class, args);
	}

}
