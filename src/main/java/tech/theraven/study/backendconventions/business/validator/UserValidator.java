package tech.theraven.study.backendconventions.business.validator;

import tech.theraven.study.backendconventions.api.dto.UserCreateDto;
import tech.theraven.study.backendconventions.common.result.Result;

public interface UserValidator extends Validator {

    String FIELD_NAME = "name";
    String FIELD_EMAIL = "email";
    String FIELD_PHONE = "phone";

    Result<Void> validate(UserCreateDto dto);

}
