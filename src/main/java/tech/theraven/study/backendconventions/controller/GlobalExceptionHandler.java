package tech.theraven.study.backendconventions.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import tech.theraven.study.backendconventions.common.HttpUtils;
import tech.theraven.study.backendconventions.common.result.Error;
import tech.theraven.study.backendconventions.common.result.ErrorType;
import tech.theraven.study.backendconventions.common.result.Result;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler {

    private final HttpUtils httpUtils;

    @ExceptionHandler(Exception.class)
    public Result<Void> generalHandler(Exception ex, HttpServletRequest request) {
        log.error(getLoggerMsg(ex.getMessage(), request), ex);
        return Result.of(new Error(ErrorType.UNEXPECTED, ex.getMessage()));
    }

    private String getLoggerMsg(String message, HttpServletRequest request) {
        return "Failed to handle request, error=" + message + ", request=" + httpUtils.toString(request);
    }

}
