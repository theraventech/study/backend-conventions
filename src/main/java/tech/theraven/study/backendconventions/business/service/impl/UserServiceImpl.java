package tech.theraven.study.backendconventions.business.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import tech.theraven.study.backendconventions.api.dto.UserCreateDto;
import tech.theraven.study.backendconventions.business.facade.ThirdPartyFacade;
import tech.theraven.study.backendconventions.business.mapper.UserMapper;
import tech.theraven.study.backendconventions.business.service.UserService;
import tech.theraven.study.backendconventions.common.result.Error;
import tech.theraven.study.backendconventions.common.result.ErrorType;
import tech.theraven.study.backendconventions.common.result.Result;
import tech.theraven.study.backendconventions.domain.User;
import tech.theraven.study.backendconventions.domain.error.ValidationError;
import tech.theraven.study.backendconventions.repository.UserRepository;

import java.util.Optional;

import static tech.theraven.study.backendconventions.business.validator.UserValidator.FIELD_EMAIL;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final ThirdPartyFacade thirdPartyFacade;

    @Override
    public Result<User> create(UserCreateDto dto) {
        Result<Void> phoneValidationResult = thirdPartyFacade.validatePhone(dto.phone());
        if (phoneValidationResult.notSuccess()) {
            Error error = phoneValidationResult.getError();
            if (error.getType() == ErrorType.VALIDATION) {
                log.debug("User. Create user, phone is not valid, dto={} error={}", dto, phoneValidationResult.getError().getDescription());
                return Result.of(new ValidationError(FIELD_EMAIL, "already exist"));
            }
            log.error("User. Create user, failed to validate phone, dto={} error={}", dto, error.getDescription());
            throw new RuntimeException("Failed to validate phone, error=" + error.getDescription());
        }

        User user = userMapper.toEntity(dto);
        user = userRepository.save(user);
        log.info("User. Create user, created, userId={}", user.getId());
        return Result.of(user);
    }

    @Override
    public User read(Long userId) {
        Optional<User> optionalUser = userRepository.findById(userId);
        if (optionalUser.isEmpty()) {
            log.warn("User. Read user, user not found, userId={}", userId);
            throw new RuntimeException("User not found, userId=" + userId);
        }
        return optionalUser.get();
    }

}
