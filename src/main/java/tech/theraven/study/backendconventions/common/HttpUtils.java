package tech.theraven.study.backendconventions.common;

import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

@Component
public class HttpUtils {

    public String toString(HttpServletRequest request) {
        StringBuffer str = request.getRequestURL();
        String queryString = request.getQueryString();
        if (queryString != null) {
            str.append("?").append(queryString);
        }
        str.append(" Headers=").append(getHeaders(request).toString());
        return str.toString();
    }

    private MultiValueMap<String, String> getHeaders(HttpServletRequest request) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = headerNames.nextElement();
            String value = request.getHeader(key);
            map.add(key, value);
        }
        return map;
    }

}
