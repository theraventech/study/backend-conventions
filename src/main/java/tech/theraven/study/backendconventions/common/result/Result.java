package tech.theraven.study.backendconventions.common.result;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import static com.fasterxml.jackson.annotation.JsonCreator.Mode.DISABLED;
import static com.fasterxml.jackson.annotation.JsonCreator.Mode.PROPERTIES;

@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class Result<T> {

    private static final Result<?> EMPTY = new Result<>(true, null, null);

    private final boolean success;
    private final Error error;
    private final T data;

    @JsonCreator(mode = PROPERTIES)
    public static <T> Result<T> of(@JsonProperty("data") T data) {
        return new Result<>(true, null, data);
    }

    @JsonCreator(mode = DISABLED)
    public static <T> Result<T> of(Error error) {
        return new Result<>(false, error, null);
    }

    public static <T> Result<T> empty() {
        @SuppressWarnings("unchecked")
        Result<T> t = (Result<T>) EMPTY;
        return t;
    }

    public static Result<Void> catching(Runnable action, Function<Throwable, Error> ifFailure) {
        try {
            action.run();
            return Result.empty();
        } catch (Throwable t) {
            return Result.of(ifFailure.apply(t));
        }
    }

    public static <T> Result<T> catching(Supplier<T> action, Function<Throwable, Error> ifFailure) {
        try {
            return Result.of(action.get());
        } catch (Throwable t) {
            return Result.of(ifFailure.apply(t));
        }
    }

    public static <T> Result<T> catchingResponse(Supplier<Result<T>> action, Function<Throwable, Error> ifFailure) {
        try {
            return action.get();
        } catch (Throwable t) {
            return Result.of(ifFailure.apply(t));
        }
    }

    public static <K> Result<K> fromOptional(Optional<K> optional, Supplier<Error> ifEmpty) {
        return optional != null && optional.isPresent()
                ? Result.of(optional.get())
                : Result.of(ifEmpty.get());
    }

    @JsonIgnore
    public boolean notSuccess() {
        return !isSuccess();
    }

    public Optional<T> dataOptional() {
        return Optional.ofNullable(data);
    }

    public <K> Result<K> map(Function<T, K> f) {
        return notSuccess() ? Result.of(error) : Result.of(f.apply(data));
    }

    public Result<T> validate(Function<T, Result<?>> f) {
        if (notSuccess()) return this;
        Result<?> result = f.apply(data);
        return result.notSuccess() ? Result.of(result.getError()) : this;
    }

    public <K> Result<K> flatMap(Function<T, Result<K>> f) {
        return notSuccess() ? Result.of(error) : f.apply(data);
    }

    public T orElseGet(T defaultData) {
        return notSuccess() ? defaultData : data;
    }

    public Result<T> orElse(Result<T> alternative) {
        return notSuccess() ? alternative : this;
    }

    public <X extends Throwable> T orElseThrow(Supplier<? extends X> exceptionSupplier) throws X {
        if (isSuccess()) {
            return data;
        }
        throw exceptionSupplier.get();
    }

    public <K> Result<K> andThen(Supplier<Result<K>> next) {
        return notSuccess() ? Result.of(error) : next.get();
    }

    public Result<T> peek(Consumer<T> consumer) {
        if (notSuccess()) return this;
        consumer.accept(data);
        return this;
    }

    public void forEach(Consumer<T> f) {
        if (isSuccess()) {
            f.accept(data);
        }
    }

}

