package tech.theraven.study.backendconventions.business.validator;

import io.micrometer.core.instrument.util.StringUtils;
import tech.theraven.study.backendconventions.common.result.Result;
import tech.theraven.study.backendconventions.domain.error.ValidationError;

public interface Validator {

    default Result<Void> validateNotNull(Object value, String field) {
        if (value == null) {
            return Result.of(new ValidationError(field, "is required"));
        }
        return Result.empty();
    }

    default Result<Void> validateNotNullAndEmpty(String value, String field) {
        return validateNotNull(value, field)
                .andThen(() -> {
                    if (StringUtils.isBlank(value)) {
                        return Result.of(new ValidationError(field, "can not be empty"));
                    }
                    return Result.empty();
                });
    }

}
