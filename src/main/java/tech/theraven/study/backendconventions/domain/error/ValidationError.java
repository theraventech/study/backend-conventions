package tech.theraven.study.backendconventions.domain.error;


import lombok.Getter;
import lombok.ToString;
import tech.theraven.study.backendconventions.common.result.Error;
import tech.theraven.study.backendconventions.common.result.ErrorType;

@Getter
@ToString
public class ValidationError extends Error {

    private final String fieldName;

    public ValidationError(String fieldName, String description) {
        super(ErrorType.VALIDATION, description);
        this.fieldName = fieldName;
    }

}

