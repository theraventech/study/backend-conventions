package tech.theraven.study.backendconventions.business.facade.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import tech.theraven.study.backendconventions.business.facade.ThirdPartyFacade;
import tech.theraven.study.backendconventions.common.result.Error;
import tech.theraven.study.backendconventions.common.result.ErrorType;
import tech.theraven.study.backendconventions.common.result.Result;

import java.net.URI;

import static org.springframework.http.HttpMethod.GET;

@Slf4j
@Component
@RequiredArgsConstructor
public class ThirdPartyFacadeImpl implements ThirdPartyFacade {

    private final RestTemplate restTemplate;
    private final ObjectMapper om;

    @Override
    public Result<Void> validatePhone(String phone) {
        UriComponentsBuilder uriBuilder = getHost()
                .pathSegment("/api/validate")
                .queryParam("phone", phone);
        return doGet(uriBuilder);
    }

    private Result<Void> doGet(UriComponentsBuilder uriBuilder) {
        URI uri = uriBuilder.build().toUri();
        try {
            String error = restTemplate.exchange(uri, GET, HttpEntity.EMPTY, String.class).getBody();
            if (StringUtils.hasLength(error)) {
                return Result.of(new Error(ErrorType.VALIDATION, error));
            }
            return Result.empty();
        } catch (Exception ex) {
            log.error("Failed to execute ThirdParty request, error=" + ex.getMessage());
            return Result.of(new Error(ErrorType.UNEXPECTED, ex.getMessage()));
        }
    }

    private UriComponentsBuilder getHost() {
        return UriComponentsBuilder.fromUriString("some url");
    }

}
