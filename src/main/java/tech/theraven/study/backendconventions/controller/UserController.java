package tech.theraven.study.backendconventions.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RestController;
import tech.theraven.study.backendconventions.api.UserApi;
import tech.theraven.study.backendconventions.api.dto.UserCreateDto;
import tech.theraven.study.backendconventions.api.dto.UserDto;
import tech.theraven.study.backendconventions.business.mapper.UserMapper;
import tech.theraven.study.backendconventions.business.service.UserService;
import tech.theraven.study.backendconventions.business.validator.UserValidator;
import tech.theraven.study.backendconventions.common.result.Result;
import tech.theraven.study.backendconventions.domain.User;

@Slf4j
@RestController
@RequiredArgsConstructor
public class UserController implements UserApi {

    private final UserService userService;
    private final UserValidator userValidator;
    private final UserMapper userMapper;

    @Override
    public Result<UserDto> create(UserCreateDto dto) {
        log.info("User. Create user, dto={}", dto);
        Result<Void> validateResult = userValidator.validate(dto);
        if (validateResult.notSuccess()) {
            log.debug("User. Create user, validation error, dto={} error={}", dto, validateResult.getError().getDescription());
            return Result.of(validateResult.getError());
        }
        return userService.create(dto)
                .map(userMapper::toDto);
    }

    @Override
    public Result<UserDto> read() {
        Long userId = this.getUserId();
        log.info("User. Read user, userId={}", userId);
        User user = userService.read(userId);
        return Result.of(userMapper.toDto(user));
    }

    private Long getUserId() {
        return (Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

}
