package tech.theraven.study.backendconventions.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tech.theraven.study.backendconventions.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {
}
