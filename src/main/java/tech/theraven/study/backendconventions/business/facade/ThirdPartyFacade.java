package tech.theraven.study.backendconventions.business.facade;

import tech.theraven.study.backendconventions.common.result.Result;

public interface ThirdPartyFacade {

    Result<Void> validatePhone(String phone);

}
