package tech.theraven.study.backendconventions.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import tech.theraven.study.backendconventions.api.dto.UserCreateDto;
import tech.theraven.study.backendconventions.api.dto.UserDto;
import tech.theraven.study.backendconventions.common.result.Result;

@RequestMapping("/api/users")
public interface UserApi {

    @PostMapping
    Result<UserDto> create(UserCreateDto userDto);

    @GetMapping
    Result<UserDto> read();

}
