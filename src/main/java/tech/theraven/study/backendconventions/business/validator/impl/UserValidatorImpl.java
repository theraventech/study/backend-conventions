package tech.theraven.study.backendconventions.business.validator.impl;

import org.springframework.stereotype.Component;
import tech.theraven.study.backendconventions.api.dto.UserCreateDto;
import tech.theraven.study.backendconventions.business.validator.UserValidator;
import tech.theraven.study.backendconventions.common.result.Result;

@Component
public class UserValidatorImpl implements UserValidator {

    @Override
    public Result<Void> validate(UserCreateDto dto) {
        return validateNotNullAndEmpty(dto.name(), FIELD_NAME)
                .andThen(() -> validateNotNullAndEmpty(dto.email(), FIELD_EMAIL))
                .andThen(() -> validateNotNullAndEmpty(dto.phone(), FIELD_PHONE));
    }

}
