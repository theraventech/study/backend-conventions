package tech.theraven.study.backendconventions.common.result;

public enum ErrorType {

    VALIDATION,
    UNEXPECTED

}
