package tech.theraven.study.backendconventions.business.service;

import tech.theraven.study.backendconventions.api.dto.UserCreateDto;
import tech.theraven.study.backendconventions.common.result.Result;
import tech.theraven.study.backendconventions.domain.User;

public interface UserService {

    Result<User> create(UserCreateDto dto);

    User read(Long userId);

}
